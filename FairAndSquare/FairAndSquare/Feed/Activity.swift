//
//  Activity.swift
//  FairAndSquare
//
//  Created by Juan Sulca on 7/29/19.
//  Copyright © 2019 Alejandro Ulloa. All rights reserved.
//

import Foundation
import Firebase

struct Activity {
    var _id: String?
    var debt: DebtInfo?
    var item: ItemInfo?
    var time: Date?
    var type: String?
    
    init?(dictionary: [String: Any]) {
        if let debt = dictionary["debt"] {
            self.debt = DebtInfo(dictionary: debt as? [String:Any] ?? [:])
        } else {
            self.debt = nil
        }
        if let item = dictionary["item"] {
            self.item = ItemInfo(dictionary: item as? [String:Any] ?? [:])
        } else {
            self.item = nil
        }
        let timeStamp = dictionary["time"] as? Timestamp? ?? nil
        self.time = timeStamp?.dateValue()
        self.type = dictionary["type"] as? String? ?? "none"
    }
}

struct DebtInfo {
    var owner: String?
    var amount: Float?
    
    init?(dictionary: [String: Any]) {
        self.amount = dictionary["amount"] as? Float? ?? 0.0
        self.owner = dictionary["owner"] as? String? ?? "none"
    }
}

struct ItemInfo {
    var owner: String?
    var name: String?
    
    init?(dictionary: [String: Any]) {
        self.name = dictionary["name"] as? String? ?? "none"
        self.owner = dictionary["owner"] as? String? ?? "none"
    }
}
