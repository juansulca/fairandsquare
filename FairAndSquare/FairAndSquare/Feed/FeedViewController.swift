//
//  FeedViewController.swift
//  FairAndSquare
//
//  Created by Juan Sulca on 7/29/19.
//  Copyright © 2019 Alejandro Ulloa. All rights reserved.
//

import UIKit
import Firebase

class FeedViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var feedTable: UITableView!
    
    private var activities = [Activity]()
    let db = Firestore.firestore()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        loadFeed()
        // Do any additional setup after loading the view.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return activities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedCell") as! FeedTableViewCell
        if activities[indexPath.row].debt?.owner != nil {
            cell.descriptionLabel.text = "\(activities[indexPath.row].debt?.amount ?? 0.0)"
            cell.nameLabel.text = activities[indexPath.row].debt?.owner
        } else {
            cell.descriptionLabel.text = "\(activities[indexPath.row].item?.name ?? "none")"
            cell.nameLabel.text = activities[indexPath.row].item?.owner
        }
        cell.dateLabel.text = getDateLabel(date: activities[indexPath.row].time ?? Date())
        cell.actionLabel.text = activities[indexPath.row].type
        return cell
    }
    
    func loadFeed() {
        let householdId = CurrentHousehold.instance.document?.documentID
        db.collection("households").document(householdId!).collection("activity").getDocuments {
            (querySnapshot, error) in
            for document in querySnapshot!.documents {
                var activity = Activity(dictionary: document.data())
                activity?._id = document.documentID
                //print(activity)
                self.activities += [activity!]
            }
            self.feedTable.reloadData()
        }
    }
    
    func getDateLabel(date: Date) -> String {
        let now = Calendar.current
        if(now.isDateInYesterday(date)){ return "Yesterday" }
        else if(now.isDateInToday(date)) { return "Today" }
        else {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/YYYY"
            return formatter.string(from: date)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
