//
//  UserInfoViewController.swift
//  FairAndSquare
//
//  Created by Alejandro Ulloa on 7/9/19.
//  Copyright © 2019 Alejandro Ulloa. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import TextFieldEffects

extension Date {
    static let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE, dd MMM yyy"
        return formatter
    } ()
    var formatted: String {
        return Date.formatter.string(from: self)
    }
}

class UserInfoViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var nameTextField: HoshiTextField!
    @IBOutlet weak var emailTextField: HoshiTextField!
    @IBOutlet weak var birthdayTextField: HoshiTextField!
    @IBOutlet weak var phoneTextField: HoshiTextField!
    @IBOutlet weak var passwordTextField: HoshiTextField!
    @IBOutlet weak var confirmPasswordTextField: HoshiTextField!
    
    let datePickerView = UIDatePicker()
    let imagePickerController = UIImagePickerController()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        datePickerView.datePickerMode = .date
        datePickerView.maximumDate = Date()
        datePickerView.addTarget(self, action: #selector(handleDatePicker), for: .valueChanged)
        imagePickerController.delegate = self
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        userImageView.layer.cornerRadius = userImageView.frame.height / 2.0
        userImageView.layer.masksToBounds = true
        
        birthdayTextField.inputView = datePickerView
    }
    
    @objc func handleDatePicker(_ datePicker: UIDatePicker) {
        birthdayTextField.text = datePickerView.date.formatted
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if passwordTextField.text! != confirmPasswordTextField.text {
            showAlertError(withMessage: "Passwords don't match!")
        }
    }

    @IBAction func cancelButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneButtonPressed(_ sender: Any) {
        if !checkfields() { return }
        createUserAuth()
    }
    
    func createUserAuth(){
        Auth.auth().createUser(withEmail: emailTextField.text!, password: passwordTextField.text!) { (authResult, error) in
            let user = authResult?.user
            if user != nil {
                
                self.storeUserPhoto(id: user!.uid)
            }else{
                self.showAlertError(withMessage: "Error in Create Account")
            }
        }
    }
    
    func storeUserInfo(id: String, imageURL: String){
        let db = Firestore.firestore()
        print("USER: \(id)")
        db.collection("users").document(id).setData([
            "_id" : id,
            "name" : self.nameTextField.text ?? "",
            "birthday": Timestamp(date: datePickerView.date),
            "email" : self.emailTextField.text ?? "",
            "phone" : self.phoneTextField.text ?? "",
            "imageURL" : imageURL
            ]
        ){ err in
            if let err = err {
                print("Error writing document: \(err)")
            } else {
                print("Document successfully written!")
                
                self.performSegue(withIdentifier: "userInfoToHouseholdMain", sender: self)
            }
        }
    }
    
    func storeUserPhoto(id: String){
        let storage = Storage.storage()
        let usersImages = storage.reference().child("user-info")
        let currentUserImage = usersImages.child("\(id).jpg")
        
        let userImage = userImageView.image
        let data = userImage?.jpegData(compressionQuality: 1)!
        
        _ = currentUserImage.putData(data!, metadata: nil) { (metadata, error) in
            currentUserImage.downloadURL { (url, error) in
                guard let downloadURL = url else {
                    return
                }
                self.storeUserInfo(id: id,imageURL: "\(downloadURL)")
            }
        }
    }
    
    func firebaseAuth(email: String, password: String) {
        Auth.auth().signIn(withEmail: email, password: password) {(result,error) in
            if let _ = error {
                self.showAlertError(withMessage: "Couldn't login")
                return
            }
        }
    }
    
    
    // PHOTO SELECTION
    
    @IBAction func addPhotoButtonPressed(_ sender: Any) {
        let alertController = UIAlertController(title: "Select Source", message: nil, preferredStyle: .actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: .default){
            (_) in
            self.imagePickerController.sourceType = .camera
            self.imagePickerController.allowsEditing = true
            self.present(self.imagePickerController, animated: true, completion: nil)
        }
        
        let photoAlbumAction = UIAlertAction(title: "Photo Album", style: .default){
            (_) in
            self.imagePickerController.sourceType = .photoLibrary
            self.imagePickerController.allowsEditing = true
            self.present(self.imagePickerController, animated: true, completion: nil)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alertController.addAction(cameraAction)
        alertController.addAction(photoAlbumAction)
        alertController.addAction(cancelAction)
        
        present(alertController,animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        
        guard let image = info[.editedImage] as? UIImage else {
            print("Error loading image")
            return
        }
        
        userImageView.image = image
        
    }
    
    // FIELD VALIDATION
    
    func checkfields() -> Bool{
        var result = true
        
        if isTextfieldEmpty(nameTextField){
            result = false
        }
        if isTextfieldEmpty(emailTextField){
            result = false
        }
        if isTextfieldEmpty(birthdayTextField){
            result = false
        }
        if isTextfieldEmpty(phoneTextField){
            result = false
        }
        if isTextfieldEmpty(passwordTextField){
            result = false
        }
        if isTextfieldEmpty(confirmPasswordTextField){
            result = false
        }
        if !result {
            showAlertError(withMessage: "Incomplete fields")
            return result
        } else if passwordTextField.text! != confirmPasswordTextField.text {
            confirmPasswordTextField.placeholderColor = UIColor.red
            showAlertError(withMessage: "Passwords don't match!")
            result = false
        }
        
        return result
    }
    
    //VERIFICATIONS
    
    func isTextfieldEmpty(_ textField: HoshiTextField) -> Bool{
        if textField.text?.isEmpty ?? true {
            textField.placeholderColor = UIColor.red
            return true
        }
        textField.placeholderColor = UIColor.lightGray
        return false
    }
    
    //ALERT MESSAGE
    
    func showAlertError(withMessage message:String){
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        
        let okAlertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alertController.addAction(okAlertAction)
        present(alertController, animated: true, completion: nil)
        
    }

}
