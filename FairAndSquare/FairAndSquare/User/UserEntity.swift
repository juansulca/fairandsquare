//
//  User.swift
//  FairAndSquare
//
//  Created by Alejandro Ulloa on 7/30/19.
//  Copyright © 2019 Alejandro Ulloa. All rights reserved.
//

import Foundation
import FirebaseFirestore

struct UserEntity {
    var _id: String?
    var birthday: Timestamp?
    var email: String?
    var households: [DocumentReference]?
    var imageURL: String?
    var name: String?
    var phone: String?
}
