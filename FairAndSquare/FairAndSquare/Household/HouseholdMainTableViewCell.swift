//
//  HouseholdMainTableViewCell.swift
//  FairAndSquare
//
//  Created by Alejandro Ulloa on 7/19/19.
//  Copyright © 2019 Alejandro Ulloa. All rights reserved.
//

import UIKit

class HouseholdMainTableViewCell: UITableViewCell {

    @IBOutlet weak var householdNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
