//
//  HouseholdInfoViewController.swift
//  FairAndSquare
//
//  Created by Alejandro Ulloa on 7/8/19.
//  Copyright © 2019 Alejandro Ulloa. All rights reserved.
//

import UIKit
import Firebase

class HouseholdMainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var householdMainTableView: UITableView!
    let db = Firestore.firestore()
    var currentHouseholdsReferences = [DocumentReference]()
    var currentHouseholds = [HouseholdEntity]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getCurrentUserHouseholds()
        CurrentHousehold.instance.document = nil
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    @IBAction func signOutButtonPressed(_ sender: Any) {
        do {
            try Auth.auth().signOut()
            self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
        } catch {
            
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if(editingStyle == .delete){
            householdMainTableView.beginUpdates()
            deleteHousehold(index: indexPath.row)
            currentHouseholds.remove(at: indexPath.row)
            householdMainTableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    func deleteHousehold(index: Int){
        db.collection("users").document(Auth.auth().currentUser!.uid).getDocument { (userDocument, error) in
            self.currentHouseholdsReferences[index].getDocument { (document, error) in
                let roomates = document?.get("roomates") as! [DocumentReference]
                if( roomates.count == 1 ){
                    self.currentHouseholdsReferences[index].delete()
                } else {
                    self.currentHouseholdsReferences[index].updateData(["roomates": FieldValue.arrayRemove([userDocument!.reference])])
                }
                
                userDocument?.reference.updateData(["households": FieldValue.arrayRemove([self.currentHouseholdsReferences[index]])])
                self.currentHouseholdsReferences.remove(at: index)
                self.householdMainTableView.endUpdates()
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentHouseholds.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "householdMainTableViewCell") as! HouseholdMainTableViewCell
        cell.householdNameLabel.text = currentHouseholds[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        for householdReference in currentHouseholdsReferences {
            householdReference.getDocument() { (document, error) in
                let documentId = document!.get("_id") as! String?
                if(documentId == self.currentHouseholds[indexPath.row]._id){
                    //print(documentId)
                    print(householdReference)
                    CurrentHousehold.instance.document = householdReference
                    self.performSegue(withIdentifier: "householdMainToMain", sender: self)
                }
            }
        }
    }
    
    //this is for testing it should go to create household
    @IBAction func addButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "householdMainToHouseholdInfo", sender: self)
    }
    
    func retrieveHouseholds(){
        self.currentHouseholds = [HouseholdEntity]()
        for householdReference in currentHouseholdsReferences {
            householdReference.getDocument() { (document, error) in
                let tempHousehold = HouseholdEntity(
                    _id: document!.get("_id") as! String?,
                    address: document!.get("address") as! String?,
                    name: document!.get("name") as! String?,
                    phone: document!.get("phone") as! String?,
                    roomates: document!.get("households") as! [DocumentReference]?
                )
                self.currentHouseholds.append(tempHousehold)
                self.currentHouseholds = self.currentHouseholds.sorted(by: { $0._id! > $1._id! })
                self.householdMainTableView.reloadData()
            }
        }
    }
    
    func getCurrentUserHouseholds(){
        let userId = Auth.auth().currentUser!.uid
        let userHouseholds = db.collection("users").document(userId)
        
        userHouseholds.addSnapshotListener { documentSnapshot, error in
            guard let document = documentSnapshot else {
                print("Error fetching document: \(error!)")
                return
            }
            guard let _ = document.data() else {
                print("Document data was empty.")
                return
            }
            self.currentHouseholdsReferences = documentSnapshot!.get("households") as! [DocumentReference]
            self.currentHouseholdsReferences = self.currentHouseholdsReferences.sorted(by: { $0.documentID > $1.documentID })
            self.retrieveHouseholds()
        }

    }
}
