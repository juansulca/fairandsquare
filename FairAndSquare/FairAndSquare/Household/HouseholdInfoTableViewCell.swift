//
//  HouseholdInfoTableViewCell.swift
//  FairAndSquare
//
//  Created by Alejandro Ulloa on 7/25/19.
//  Copyright © 2019 Alejandro Ulloa. All rights reserved.
//

import UIKit

class HouseholdInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var roommateNameLabel: UILabel!
    @IBOutlet weak var rommateImageView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
