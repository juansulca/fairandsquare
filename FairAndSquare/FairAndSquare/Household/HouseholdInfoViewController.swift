//
//  HouseholdInfoViewController.swift
//  FairAndSquare
//
//  Created by Alejandro Ulloa on 7/23/19.
//  Copyright © 2019 Alejandro Ulloa. All rights reserved.
//

import UIKit
import TextFieldEffects
import Firebase
import FirebaseAuth
import Kingfisher

class HouseholdInfoViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var nameTextField: HoshiTextField!
    @IBOutlet weak var addressTextField: HoshiTextField!
    @IBOutlet weak var phoneTextField: HoshiTextField!
    @IBOutlet weak var roomatesTableView: UITableView!
    
    var roommateReferences = [DocumentReference]()
    var roommateEntities = [UserEntity]()
    let db = Firestore.firestore()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setDoneButton()
        let currentUserReference = db.collection("users").document(Auth.auth().currentUser!.uid)
        
        roommateReferences.append(currentUserReference)
        addCurrentUserToRoomates()
    }
    
    func addCurrentUserToRoomates(){
        db.collection("users").document(Auth.auth().currentUser!.uid).getDocument { (document, error) in
            if let document = document, document.exists {
                print("User obtained")
                var roomate = UserEntity()
                roomate._id = document.get("_id") as! String
                roomate.imageURL = document.get("imageURL") as! String
                roomate.name = document.get("name") as! String
                self.roommateEntities.append(roomate)
                self.roomatesTableView.reloadData()
            } else {
                print("Document does not exist")
            }
        }
    }
    
    @IBAction func addParticipantsButtonPressed(_ sender: Any) {
        addRoommateAlert()
        
    }
    
    //Table
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return roommateEntities.count
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if(editingStyle == .delete){
            roomatesTableView.beginUpdates()
            roommateEntities.remove(at: indexPath.row)
            roommateReferences.remove(at: indexPath.row)
            roomatesTableView.deleteRows(at: [indexPath], with: .fade)
            roomatesTableView.endUpdates()
        }
        for user in roommateEntities {
            print(user._id)
        }
        for user in roommateReferences {
            print(user.documentID)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "householdInfoTableViewCell") as! HouseholdInfoTableViewCell
        cell.roommateNameLabel.text = roommateEntities[indexPath.row].name
        cell.rommateImageView.kf.setImage(with: URL( string: roommateEntities[indexPath.row].imageURL!))
        return cell
    }
    
    //Button
    
    func setDoneButton(){
        
        let doneButton: UIButton = UIButton(type: UIButton.ButtonType.system)
        doneButton.setTitle("Done", for: UIControl.State.normal)
        doneButton.setTitleColor(UIColor(red: 0/255.0, green: 122/255.0, blue: 255/255.0, alpha: 1), for: UIControl.State.normal) //alpha is transparency
        doneButton.addTarget(self, action: #selector(doneButtonTapped), for: UIControl.Event.touchUpInside)
        
        let barButton = UIBarButtonItem(customView: doneButton)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func doneButtonTapped(){
        if !checkFields() { return }
        createHousehold()
    }
    
    // Create new household
    
    func createHousehold(){
        
        var ref: DocumentReference? = nil
        ref = db.collection("households").addDocument(data: [
            "name" : self.nameTextField.text ?? "",
            "address" : self.addressTextField.text ?? "",
            "phone" : self.phoneTextField.text ?? "",
            "roomates": self.roommateReferences
            ]
        ){ err in
            if let err = err {
                print("Error writing document: \(err)")
            } else {
                print("Document \(ref?.documentID ?? "NA") successfully written!")
                ref?.updateData([
                    "_id" : ref!.documentID
                ]) { err in
                    if let err = err {
                        print("Error updating document: \(err)")
                    } else {
                        print("Document successfully updated")
                        self.updateUsersHouseholds(household: ref!)
                        self.navigationController?.popViewController(animated: true)
                    }
                }
                
            }
        }
    }
    
    func updateUsersHouseholds(household: DocumentReference){
        for user in self.roommateReferences {
            user.updateData([
                "households": FieldValue.arrayUnion([household])
            ]) { err in
                if let err = err {
                    print("Error updating document: \(err)")
                } else {
                    print("User \(user.documentID) successfully updated")
                    //self.performSegue(withIdentifier: "userInfoToHouseholdMain", sender: self)
                }
            }
        }
    }
    
    // FIELD VALIDATION
    
    func checkFields() -> Bool{
        var result = true
        
        if isTextfieldEmpty(nameTextField){
            result = false
        }
        if isTextfieldEmpty(phoneTextField){
            result = false
        }
        if isTextfieldEmpty(phoneTextField){
            result = false
        }
        if !result {
            showAlertError(withMessage: "Incomplete fields")
            return result
        }
        return result
    }
    
    //VERIFICATIONS
    
    func isTextfieldEmpty(_ textField: HoshiTextField) -> Bool{
        if textField.text?.isEmpty ?? true {
            textField.placeholderColor = UIColor.red
            return true
        }
        textField.placeholderColor = UIColor.lightGray
        return false
    }
    
    //ALERT MESSAGE
    
    func showAlertError(withMessage message:String){
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        
        let okAlertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alertController.addAction(okAlertAction)
        present(alertController, animated: true, completion: nil)
        
    }
    
    //ADD ROOMATES
    
    func addRoommateAlert() {
        let inputAlert = UIAlertController(title: "Add Roommate", message: "Please enter roommate email", preferredStyle: UIAlertController.Style.alert)
        let inputAction = UIAlertAction(title: "Add", style: .default) { (alertAction) in
            let textField = inputAlert.textFields![0] as UITextField
            self.getRoommateReference(enteredEmail: textField.text ?? "")
        }
        inputAlert.addTextField { (textField) in
            textField.placeholder = "e-mail"
            textField.keyboardType = .emailAddress
        }
        inputAlert.addAction(inputAction)
        
        self.present(inputAlert, animated: true, completion: nil)
    }
    
    func getRoommateReference(enteredEmail: String){
        db.collection("users").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    let email = document.get("email") as! String
                    print(email)
                    if(enteredEmail == email){
                        print("EMAIL FOUND")
                        if(!self.roommateReferences.contains(document.reference)){
                            self.roommateReferences.append(document.reference)
                            self.getRoommateObject(roommateReference: document)
                            
                        }
                        return
                    }
                }
                self.showAlertError(withMessage: "E-mail was not found")
            }
        }
    }
    
    func getRoommateObject(roommateReference: QueryDocumentSnapshot){
        var roomate = UserEntity()
        roomate._id = roommateReference.get("_id") as! String
        roomate.imageURL = roommateReference.get("imageURL") as! String
        roomate.name = roommateReference.get("name") as! String
        roommateEntities.append(roomate)
        self.roomatesTableView.reloadData()
    }

}
