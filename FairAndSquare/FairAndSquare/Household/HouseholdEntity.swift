//
//  Household.swift
//  FairAndSquare
//
//  Created by Alejandro Ulloa on 7/16/19.
//  Copyright © 2019 Alejandro Ulloa. All rights reserved.
//

import Foundation
import FirebaseFirestore

struct HouseholdEntity{
    var _id: String?
    var address: String?
    var name: String?
    var phone: String?
    var roomates: [DocumentReference]?
}

class CurrentHousehold {
    var document: DocumentReference?
    
    static var instance: CurrentHousehold = {
        let instance = CurrentHousehold()
        return instance
    }()
    
    private init() {
        
    }
    
    
}
