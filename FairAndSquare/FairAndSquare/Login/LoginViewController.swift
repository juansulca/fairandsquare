//
//  LoginViewController.swift
//  FairAndSquare
//
//  Created by Juan Sulca on 7/2/19.
//  Copyright © 2019 Alejandro Ulloa. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase
import FirebaseFirestore

class LoginViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let db = Firestore.firestore()
        
        guard let currentUser = Auth.auth().currentUser else {
            return
        }
        
        let userRef = db.collection("users").document(currentUser.uid)
        userRef.getDocument { (snapshot, error) in
            self.performSegue(withIdentifier: "loginToHouseholds", sender: self)
        }
    }
    
    @IBAction func loginButtonPressed(_ sender: Any) {
        firebaseAuth(email: emailTextField.text!, passwd: passwordTextField.text!)
    }
    
    @IBAction func signupButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "loginToNewUser", sender: self)
    }
    
    func showAlertError(error: String){
        let alertView = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
        let okAlertAction = UIAlertAction(title: "OK", style: .default) { [unowned self](_) in
            self.emailTextField.text = ""
            self.passwordTextField.text = ""
        }
        alertView.addAction(okAlertAction)
        present(alertView, animated: true, completion: nil)
    }
    
    func firebaseAuth(email: String, passwd: String) {
        Auth.auth().signIn(withEmail: email, password: passwd){(result, error) in
            if let _ = error {
                //print(error!.localizedDescription)
                self.showAlertError(error: error!.localizedDescription)
                return
            }
            self.performSegue(withIdentifier: "loginToHouseholds", sender: self)
        }
    }

}
