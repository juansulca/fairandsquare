//
//  Debt.swift
//  FairAndSquare
//
//  Created by Sebastian Poveda on 7/19/19.
//  Copyright © 2019 Alejandro Ulloa. All rights reserved.
//

import Foundation
import FirebaseFirestore
import ObjectMapper

//TODO: make participants array


struct Debt {
    var id: String?
    var name: String?
    var amount: Double?
    var owner: DebtOwner?
    var participants: DocumentReference?
    var status: String?
    
    
    init?(dictionary: [String: Any]) {
        self.name = dictionary["name"] as! String?
        self.amount = dictionary["amount"] as! Double?
        self.owner = DebtOwner(dictionary: dictionary["owner"] as! [String: Any])
        self.participants = dictionary["participants"] as! DocumentReference?
        self.status = dictionary["status"] as! String?
    }
}

struct DebtOwner {
    var name: String?
    var reference: DocumentReference?
    
    init?(dictionary: [String: Any]) {
        self.name = dictionary["name"] as! String?
        self.reference = dictionary["reference"] as! DocumentReference?
    }
}

