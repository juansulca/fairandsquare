//
//  DebtAddParticipantViewController.swift
//  FairAndSquare
//
//  Created by Sebastian Poveda on 8/1/19.
//  Copyright © 2019 Alejandro Ulloa. All rights reserved.
//

import UIKit
import FirebaseFirestore

protocol DisplayViewControllerDelegate : NSObjectProtocol{
    func doSomethingWith(user: UserEntity)
}


class DebtAddParticipantViewController: UIViewController,
UITableViewDelegate, UITableViewDataSource{

    @IBOutlet weak var AddRoommateTable: UITableView!
    
    var participants = [UserEntity]()
    var roommates = [UserEntity]()
    var user = UserEntity()
    
    weak var delegate : DisplayViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        print (participants)
        if participants.isEmpty {
            getAllRoommates()
        }else {
            getRoommates()
        }

    }
    
    func compareUsers(tempUser: UserEntity){
        for user in participants {
            print("Test5")
            if user._id != tempUser._id{
                print("Test6")
                self.roommates.append(tempUser)
            }
        }
        self.AddRoommateTable.reloadData()
    }
    
    
    func getRoommates(){
        participants.removeAll()
        let db = Firestore.firestore()
        _ = CurrentHousehold.instance.document?.documentID
        db.collection("users").getDocuments{ (snapshot, error) in
            for document in snapshot!.documents{
                var tempUser = UserEntity()
                tempUser._id = document.get("_id") as! String?
                tempUser.name = document.get("name") as! String?
                tempUser.imageURL = document.get("imageURL") as! String?
                self.compareUsers(tempUser: tempUser)
            }
        }
    }
    
    func getAllRoommates(){
        let db = Firestore.firestore()
        _ = CurrentHousehold.instance.document?.documentID
        db.collection("users").getDocuments{ (snapshot, error) in
            for document in snapshot!.documents{
                var tempUser = UserEntity()
                tempUser._id = document.get("_id") as! String?
                tempUser.name = document.get("name") as! String?
                tempUser.imageURL = document.get("imageURL") as! String?
                self.roommates.append(tempUser)
            }
            self.AddRoommateTable.reloadData()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        user = roommates[indexPath.row]
        popViewController()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddRoommateCell") as! DebtRoommateTableViewCell
        cell.userId = roommates[indexPath.row]._id
        cell.AddRoommateLabel.text = roommates[indexPath.row].name
        cell.AddRoommateImage.kf.setImage(with: URL( string: roommates[indexPath.row].imageURL!))
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return roommates.count
    }

    
    func popViewController() {
        if let delegate = delegate{
            delegate.doSomethingWith(user: user)
        }
        self.navigationController?.popViewController(animated: true)
    }

}
