//
//  DebtsViewController.swift
//  FairAndSquare
//
//  Created by Sebastian Poveda on 7/9/19.
//  Copyright © 2019 Alejandro Ulloa. All rights reserved.
//

import UIKit
import Firebase

class DebtsViewController: UIViewController,  UITableViewDelegate, UITableViewDataSource, DisplayViewControllerDelegateDebt{
    
    
    @IBOutlet weak var DebtsTable: UITableView!
    
    
    let db = Firestore.firestore()
    
    var debts = [Debt]()
    var selectedDebtId: String?
    var incommingId = ""
    

    override func viewDidLoad() {
        super.viewDidLoad()
        let button: UIBarButtonItem?
            button = UIBarButtonItem(title: "Add", style: .done, target: self, action: #selector(addButtonTapped))
        self.navigationItem.rightBarButtonItem = button

        loadDebts()
        
        print(CurrentHousehold.instance.document)
        //configSearchBar ()
    }
    
    //override func viewDidAppear(_ animated: Bool) {
       // var x = 0
        //for debt in debts{
           // x = x + 1
           // if debt.id == selectedDebtId{
           //     debts.remove(at: x)
         //   }
      //  }
     //   self.DebtsTable.reloadData()
  //  }
    
    @objc func addButtonTapped(){
        selectedDebtId = nil
        performSegue(withIdentifier: "toDebtInfoSegue", sender: self)
    }
    

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedDebtId = debts[indexPath.row].id
        performSegue(withIdentifier: "toDebtInfoSegue", sender: self)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return debts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DebtsCell") as! DebtViewCell
        cell.debtId = debts[indexPath.row].id
        cell.DebtsNameLabel.text = debts[indexPath.row].name
        cell.DebtsAmountLabel.text = "\(debts[indexPath.row].amount ?? 0.0)"
        return cell
    }
    
    func loadDebts(){
        let householdId = CurrentHousehold.instance.document?.documentID
        db.collection("households").document(householdId!).collection("debts").getDocuments{
            (querySnapshot, error) in
            for document in querySnapshot!.documents{
                let status = document.get("status") as? String ?? "payed"
                if status == "pending"{
                    var debt = Debt(dictionary: document.data())
                    debt?.id = document.documentID
                    self.debts += [debt!]
                }
            }
            self.DebtsTable.reloadData()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toDebtInfoSegue" {
            let destination = segue.destination as! DebtInfoViewController
            destination.debtId = selectedDebtId
        }
    }
    
    func reloadData(id: String) {
        var x = 0
        incommingId = id
        print(incommingId)
        for debt in debts{
            x = x + 1
            if debt.id == id{
                debts.remove(at: x)
            }
        }
        self.DebtsTable.reloadData()
    }

}
