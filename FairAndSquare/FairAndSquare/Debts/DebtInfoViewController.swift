//
//  DebtInfoViewController.swift
//  FairAndSquare
//
//  Created by Sebastian Poveda on 7/18/19.
//  Copyright © 2019 Alejandro Ulloa. All rights reserved.
//

import UIKit
import TextFieldEffects
import Firebase

protocol DisplayViewControllerDelegateDebt : NSObjectProtocol{
    func reloadData(id: String)
}

class DebtInfoViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, DisplayViewControllerDelegate{

    @IBOutlet weak var DebtParticipantTable: UITableView!
    @IBOutlet weak var DebtName: HoshiTextField!
    @IBOutlet weak var DebtAmountTextField: HoshiTextField!
    @IBOutlet weak var DebtButtonGroup: UISegmentedControl!
    @IBOutlet weak var AddHouseHoldButton: UIButton!
    @IBOutlet weak var AddParticipantButton: UIButton!
    @IBOutlet weak var ButtonGroup: UISegmentedControl!
    
    weak var delegate : DisplayViewControllerDelegateDebt?
    
    var idBtn: Int = 0
    
    @IBAction func DebtInfoAddHouseHoldButton(_ sender: Any) {
        self.getRoommates()
    }
    
    @IBAction func DebtInfoAddParticipantButton(_ sender: Any) {
        
    }
    
    @IBAction func segmentedControl(_ sender: Any) {
        switch self.ButtonGroup.selectedSegmentIndex
        {
        case 0:
            idBtn = 0
        case 1:
            idBtn = 1
        case 2:
            idBtn = 2
        default:
            idBtn = 0
            break
        }
    }
    
    var debtId: String!
    var debtData:Debt?
    var user = UserEntity()
    var participants = [UserEntity]()
    var userReference: DocumentReference?
    var incommingUser = UserEntity()
    var idDebt = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(incommingUser)
        
        if let debtId = debtId {
            loadDebtData(id: debtId)
            AddHouseHoldButton.isHidden = true
            AddParticipantButton.isHidden = true
            ButtonGroup.isHidden = true
            print(incommingUser)
            idDebt = debtId
            addPayButton()
        } else {
            addSaveButton()
            print(incommingUser)
            
        }
    }
    

    
    func loadDebtData(id: String){
        let db = Firestore.firestore()
        let householdId = CurrentHousehold.instance.document?.documentID
        db.collection("households").document(householdId!).collection("debts").document(id).getDocument { (snapshot, error) in
            self.debtData = Debt(dictionary: snapshot!.data()!)
            self.DebtName.text = self.debtData?.name
            self.DebtAmountTextField.text = String(format:"%.2f", self.debtData!.amount!)
            self.userReference = self.debtData!.participants!
            self.getUserReferences()
            //self.DebtParticipantTable.reloadData()
        }
    }
    
    func getUserReferences(){
        userReference?.getDocument{ (result, error) in
            if let result = result, result.exists {
                print("User obtained")
                self.user._id = result.get("_id") as! String?
                self.user.name = result.get("name") as! String?
                self.user.imageURL = result.get("imageURL") as! String?
                self.participants.append(self.user)
                self.DebtParticipantTable.reloadData()
            }else{
                print("Document doesnt exist")
            }
        }
    }
    
    
    func getRoommates(){
        participants.removeAll()
        let db = Firestore.firestore()
        _ = CurrentHousehold.instance.document?.documentID
        db.collection("users").getDocuments{ (snapshot, error) in
            for document in snapshot!.documents{
                var tempUser = UserEntity()
                tempUser._id = document.get("_id") as! String?
                tempUser.name = document.get("name") as! String?
                tempUser.imageURL = document.get("imageURL") as! String?
                self.participants.append(tempUser)
                self.DebtParticipantTable.reloadData()
            }
        }
    }
    
    func addSaveButton(){
        let button = UIBarButtonItem(title: "Save", style: .done, target: self, action: #selector(saveButtonTapped))
        self.navigationItem.rightBarButtonItem = button
    }
    
    func addPayButton(){
        let button = UIBarButtonItem(title: "Pay", style: .done, target: self, action: #selector(payButtonTapped))
        self.navigationItem.rightBarButtonItem = button
    }
    
    @objc func payButtonTapped(){
        let db = Firestore.firestore()
        let householdId = CurrentHousehold.instance.document?.documentID
        let itemReference = db.collection("households").document(householdId ?? "").collection("debts").document(idDebt)
        itemReference.updateData(["status": "payed"])
        
            self.popViewController()
    }
    
    @objc func saveButtonTapped(){
        
        let db = Firestore.firestore()
        let householdId = CurrentHousehold.instance.document?.documentID
        let userId = Auth.auth().currentUser?.uid
        switch idBtn{
        case 0:
            db.collection("users").document(userId ?? "").getDocument {(snapshot, error) in
                print(snapshot!.reference)
                print("split")
                let collection = db.collection("households").document(householdId ?? "").collection("debts")
                for user in self.participants{
                    var idUpdate = ""
                    let data: [String : Any] = [
                        "_id": "",
                        "amount": ((Double(self.DebtAmountTextField.text!) ?? 0.0)/(Double(self.participants.count))),
                        "name": self.DebtName.text ?? "none",
                        "owner": [
                            "name" : snapshot?.get("name") as? String ?? "",
                            "reference": snapshot!.reference
                        ],
                        "participants": db.collection("users").document(user._id!),
                        "status": "pending"
                    ]
                    collection.addDocument(data: data).getDocument{
                        (result, error) in
                        idUpdate = result!.documentID
                        
                        db.collection("households").document(householdId ?? "").collection("debts").document(idUpdate).updateData(["_id": idUpdate])
                    }

                }
                    self.popViewController()
            }
        case 1:
            print("1")
            db.collection("users").document(userId ?? "").getDocument {(snapshot, error) in
                print(snapshot!.reference)
                let collection = db.collection("households").document(householdId ?? "").collection("debts")
                for user in self.participants{
                    var idUpdate = ""
                    let data: [String : Any] = [
                        "_id": "",
                        "amount": ((Double(self.DebtAmountTextField.text!) ?? 0.0)/(Double(self.participants.count))),
                        "name": self.DebtName.text ?? "none",
                        "owner": [
                            "name" : snapshot?.get("name") as? String ?? "",
                            "reference": snapshot!.reference
                        ],
                        "participants": db.collection("users").document(user._id!),
                        "status": "pending"
                    ]
                    collection.addDocument(data: data).getDocument{
                        (result, error) in
                        idUpdate = result!.documentID
                                          db.collection("households").document(householdId ?? "").collection("debts").document(idUpdate).updateData(["_id": idUpdate])
                        
                    }

                }
                
                    self.popViewController()
                
            }

            
        case 2:
            print("2")
            db.collection("users").document(userId ?? "").getDocument {(snapshot, error) in
                let collection = db.collection("households").document(householdId ?? "").collection("debts")
                for user in self.participants{
                    var idUpdate = ""
                    if(self.participants.count == 1){
                        
                        let data: [String : Any] = [
                            "_id": "",
                            "amount": ((Double(self.DebtAmountTextField.text!) ?? 0.0)/(Double(self.participants.count))),
                            "name": self.DebtName.text ?? "none",
                            "owner": [
                                "name" : user.name!,
                                "reference": db.collection("users").document(user._id!)
                            ],
                            "participants": snapshot!.reference,
                            "status": "pending"
                        ]
                        collection.addDocument(data: data).getDocument{
                            (result, error) in
                            idUpdate = result!.documentID
                                              db.collection("households").document(householdId ?? "").collection("debts").document(idUpdate).updateData(["_id": idUpdate])
                        }
                    }
  
                    
                }
                
                    self.popViewController()
            }
            
            
        default:
            print("No valio")
            break
        }
    }
    
    func createNewDebt(debt: Debt){
       
    }
    
    @objc func addButtonTapped(){
        // some save item logic
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return participants.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DebtParticipantCell") as! DebtParticipantTableViewCell
        cell.debtParticipantId = participants[indexPath.row]._id
        cell.DebtUserParticipantLabel.text = participants[indexPath.row].name
        cell.DebtUserParticipantImage.kf.setImage(with: URL( string: participants[indexPath.row].imageURL!))
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.identifier == "toAddParticipantSegue"){
            let destination = segue.destination as! DebtAddParticipantViewController
            destination.delegate = self
            destination.participants = participants
        }
        
    }
    
    func doSomethingWith(user: UserEntity) {
       incommingUser = user
        self.participants.append(incommingUser)
        self.DebtParticipantTable.reloadData()
        print(user)
        print("fuck")
    }
    
    
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if(editingStyle == .delete){
            DebtParticipantTable.beginUpdates()
            participants.remove(at: indexPath.row)
            DebtParticipantTable.deleteRows(at: [indexPath], with: .fade)
            DebtParticipantTable.endUpdates()
        }
    }
    
    func popViewController() {
        if let delegate = self.delegate{
            delegate.reloadData(id:self.idDebt)
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
    
}

