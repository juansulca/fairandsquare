//
//  DebtRoommateTableViewCell.swift
//  FairAndSquare
//
//  Created by Sebastian Poveda on 8/1/19.
//  Copyright © 2019 Alejandro Ulloa. All rights reserved.
//

import UIKit

class DebtRoommateTableViewCell: UITableViewCell {

    @IBOutlet weak var AddRoommateImage: UIImageView!
    @IBOutlet weak var AddRoommateLabel: UILabel!
    
    var userId: String!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
