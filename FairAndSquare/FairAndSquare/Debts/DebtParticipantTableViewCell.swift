//
//  DebtParticipantTableViewCell.swift
//  FairAndSquare
//
//  Created by Sebastian Poveda on 7/18/19.
//  Copyright © 2019 Alejandro Ulloa. All rights reserved.
//

import UIKit
import FirebaseFirestore

class DebtParticipantTableViewCell: UITableViewCell {

    @IBOutlet weak var DebtUserParticipantImage: UIImageView!
    @IBOutlet weak var DebtUserParticipantLabel: UILabel!
    @IBOutlet weak var DebtRemoveImage: UIImageView!
    
    var debtParticipantId: String!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    


}
