//
//  DebtViewCell.swift
//  FairAndSquare
//
//  Created by Sebastian Poveda on 7/9/19.
//  Copyright © 2019 Alejandro Ulloa. All rights reserved.
//

import UIKit

class DebtViewCell: UITableViewCell {

    @IBOutlet weak var DebtsNameLabel: UILabel!
    @IBOutlet weak var DebtsAmountLabel: UILabel!
    @IBOutlet weak var DebtsChevronIcon: UIImageView!
    
    var debtId: String!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
