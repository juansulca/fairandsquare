//
//  SettingsViewController.swift
//  FairAndSquare
//
//  Created by Juan Sulca on 7/9/19.
//  Copyright © 2019 Alejandro Ulloa. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore
import Kingfisher

class SettingsViewController: UIViewController {

    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userBirthdayLabel: UILabel!
    @IBOutlet weak var userEmailLabel: UILabel!
    @IBOutlet weak var userPhoneLabel: UILabel!
    @IBOutlet weak var householdNameLabel: UILabel!
    @IBOutlet weak var householdAddressLabel: UILabel!
    @IBOutlet weak var householdPhoneLabel: UILabel!
    
    
    let db = Firestore.firestore()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadUserData()
        loadHouseholdData()
    }
    
    func loadUserData(){
        let userId = Auth.auth().currentUser?.uid
        db.collection("users").document(userId ?? "").getDocument {
            (document, error) in
            let user = self.getUserData(document: document!)
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/YYYY"
            let birthday = formatter.string(from: user.birthday?.dateValue() ?? Date())
            self.userNameLabel.text = user.name
            self.userBirthdayLabel.text = birthday
            self.userEmailLabel.text = user.email
            self.userPhoneLabel.text = user.phone
            self.userImageView.kf.setImage(with: URL(string: user.imageURL ?? ""))
        }
    }
    
    func getUserData(document: DocumentSnapshot) -> UserEntity {
        var user = UserEntity()
        user._id = document.documentID
        user.birthday = document.get("birthday") as? Timestamp ?? nil
        user.email = document.get("email") as? String? ?? "Not given"
        user.name = document.get("name") as? String? ?? "Not given"
        user.imageURL = document.get("imageURL") as? String? ?? ""
        user.phone = document.get("phone") as? String? ?? ""
        return user
    }
    
    func loadHouseholdData(){
        CurrentHousehold.instance.document?.getDocument{
            (document, error) in
            self.householdNameLabel.text = document?.get("name") as? String
            self.householdAddressLabel.text = document?.get("address") as? String
            self.householdPhoneLabel.text = document?.get("phone") as? String
        }
        
    }
    
    @IBAction func allHouseholdsButtonPressed(_ sender: Any) {
        self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
    }
    
    func deleteHousehold(){
        db.collection("users").document(Auth.auth().currentUser!.uid).getDocument { (userDocument, error) in
            CurrentHousehold.instance.document!.getDocument { (document, error) in
                let roomates = document?.get("roomates") as! [DocumentReference]
                if( roomates.count == 1 ){
                    CurrentHousehold.instance.document!.delete()
                } else {
                    CurrentHousehold.instance.document!.updateData(["roomates": FieldValue.arrayRemove([userDocument!.reference])])
                }
                userDocument?.reference.updateData(["households": FieldValue.arrayRemove([CurrentHousehold.instance.document!])])
            }
            self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func exitHouseholdButtonPressed(_ sender: Any) {
        deleteHousehold()
        
    }
    
    
}
