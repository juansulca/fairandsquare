//
//  Items.swift
//  FairAndSquare
//
//  Created by Juan Sulca on 7/19/19.
//  Copyright © 2019 Alejandro Ulloa. All rights reserved.
//

import Foundation
import Firebase

struct Item {
    var _id: String?
    var name: String?
    var description: String?
    var owner: ItemOwner?
    var location: Location?
    var status: String?
    
    init?(dictionary: [String: Any]) {
        self._id = dictionary["_id"] as? String? ?? nil
        self.name = dictionary["name"] as? String? ?? "none"
        self.description = dictionary["description"] as? String? ?? "none"
        self.owner = ItemOwner(dictionary: dictionary["owner"] as? [String: Any] ?? [:])
        self.location = Location(dictionary: dictionary["location"] as? [String: Any] ?? [:])
        self.status = dictionary["status"] as? String? ?? ""
    }
}

struct Location {
    var name: String?
    var lat: Double?
    var lng: Double?
    
    init?(dictionary: [String: Any]) {
        self.name = dictionary["name"] as? String? ?? "none"
        self.lat = dictionary["lat"] as? Double? ?? 0.0
        self.lng = dictionary["lng"] as? Double? ?? 0.0
    }
}

struct ItemOwner {
    var name: String?
    var reference: DocumentReference?
    
    init?(dictionary: [String: Any]) {
        self.name = dictionary["name"] as? String? ?? "none"
        self.reference = dictionary["reference"] as? DocumentReference? ?? nil
    }
}
