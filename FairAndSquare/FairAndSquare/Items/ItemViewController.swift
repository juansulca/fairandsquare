//
//  ItemViewController.swift
//  FairAndSquare
//
//  Created by Juan Sulca on 7/16/19.
//  Copyright © 2019 Alejandro Ulloa. All rights reserved.
//

import UIKit
import MapKit
import Firebase

class ItemInfoViewController: UIViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var itemDescriptionText: UITextField!
    @IBOutlet weak var itemNameText: UITextField!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var locationText: UITextField!
    
    var itemId: String!
    
    var pin: MKPointAnnotation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadMap()
        if let itemId = itemId {
            loadItemData(id: itemId)
        } else {
            addSaveButton()
        }
    }
    
    func loadMap(){
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        gestureRecognizer.delegate = self
        mapView.addGestureRecognizer(gestureRecognizer)
    }
    
    @objc func handleTap(gestureReconizer: UIGestureRecognizer){
        let location = gestureReconizer.location(in: mapView)
        let coordinate = mapView.convert(location,toCoordinateFrom: mapView)
        print(coordinate.latitude, coordinate.latitude)
        
        movePin(coordinate: coordinate)
        
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude), completionHandler: {places,error in
            /*if error? {
                return
            }*/
            self.locationText.text = "\(places?[0].name ?? "not found"), \(places?[0].locality ?? "not found"), \(places?[0].country ?? "not found")"
        })
    }
    
    func movePin(coordinate: CLLocationCoordinate2D){
        if pin == nil {
            pin = MKPointAnnotation()
        }
        pin?.coordinate = coordinate
        mapView.addAnnotation(pin!)
    }
    
    func zoomTo(coordinate: CLLocationCoordinate2D){
        let span = MKCoordinateSpan(latitudeDelta: 0.008, longitudeDelta: 0.008)
        let region = MKCoordinateRegion(center: coordinate, span: span)
        
        mapView.setRegion(region, animated: true)
    }
    
    func geocode(latitude: Double, longitude: Double, completion: @escaping (_ placemark: [CLPlacemark]?, _ error: Error?) -> Void)  {
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: latitude, longitude: longitude), completionHandler: completion)
    }
    
    func loadItemData(id: String){
        let db = Firestore.firestore()
        db.collection("households").document("n97qnKjY6tclesZi4LgW").collection("items").document(id).getDocument { (snapshot, error) in
            if let item = snapshot.flatMap({
                $0.data().flatMap({(data) in
                    return Item(dictionary: data)
                })
            }){
                let coordinate = CLLocationCoordinate2D(latitude: item.location?.lat ?? 0.0, longitude: item.location?.lng ?? 0.0)
                self.movePin(coordinate: coordinate)
                self.zoomTo(coordinate: coordinate)
                self.locationText.text = item.location?.name ?? "unknown"
                self.itemNameText.text = item.name ?? "unknown"
                self.itemDescriptionText.text = item.description ?? "unknown"
            }
        }
    }
    
    func addSaveButton(){
        let button = UIBarButtonItem(title: "Save", style: .done, target: self, action: #selector(saveButtonTapped))
        self.navigationItem.rightBarButtonItem = button
    }
    
    @objc func saveButtonTapped(){
        // some save item logic
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
