//
//  ItemsViewCell.swift
//  FairAndSquare
//
//  Created by Sebastian Poveda on 7/9/19.
//  Copyright © 2019 Alejandro Ulloa. All rights reserved.
//

import UIKit

class ItemsViewCell: UITableViewCell {

    @IBOutlet weak var ItemsNameLabel: UILabel!
    @IBOutlet weak var ItemsOwnerLabel: UILabel!
    @IBOutlet weak var ItemsCheckIcon: UIImageView!
    
    var itemId: String!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
