//
//  ItemsViewController.swift
//  FairAndSquare
//
//  Created by Sebastian Poveda on 7/9/19.
//  Copyright © 2019 Alejandro Ulloa. All rights reserved.
//

import UIKit
import Firebase

class ItemsMainViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var itemsTable: UITableView!
    
    var items = [Item]()
    var selectedItemId: String?
    
    let db = Firestore.firestore()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let button = UIBarButtonItem(title: "Add", style: .done, target: self, action: #selector(addButtonTapped))
        self.navigationItem.rightBarButtonItem = button
        loadItems()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedItemId = items[indexPath.row]._id
        performSegue(withIdentifier: "pendingItemsToItem", sender: self)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemsCell") as! ItemsViewCell
        cell.itemId = items[indexPath.row]._id
        cell.ItemsNameLabel.text = items[indexPath.row].name
        cell.ItemsOwnerLabel.text = items[indexPath.row].owner?.name
        return cell
    }
    
    @objc func addButtonTapped(){
        selectedItemId = nil
        performSegue(withIdentifier: "pendingItemsToItem", sender: self)
    }
    
    func loadItems() {
        self.items.removeAll()
        let householdId = CurrentHousehold.instance.document?.documentID
        let itemsReference = db.collection("households").document(householdId!).collection("items")
        itemsReference.whereField("status", isEqualTo: "pending").getDocuments {
            (querySnapshot, error) in
            self.items.removeAll()
            for document in querySnapshot!.documents {
                var item = Item(dictionary: document.data())
                item?._id = document.documentID
                self.items += [item!]
            }
            self.itemsTable.reloadData()
        }
        itemsReference.whereField("status", isEqualTo: "pending").addSnapshotListener {(snapshot, error) in
            self.items.removeAll()
            for document in snapshot!.documents {
                var item = Item(dictionary: document.data())
                item?._id = document.documentID
                self.items += [item!]
            }
            self.itemsTable.reloadData()
        }
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "pendingItemsToItem" {
            let destination = segue.destination as! ItemInfoViewController
            destination.itemId = selectedItemId
        }
    }

}
