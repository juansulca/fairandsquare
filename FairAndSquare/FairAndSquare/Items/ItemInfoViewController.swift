//
//  ItemViewController.swift
//  FairAndSquare
//
//  Created by Juan Sulca on 7/16/19.
//  Copyright © 2019 Alejandro Ulloa. All rights reserved.
//

import UIKit
import MapKit
import Firebase

class ItemInfoViewController: UIViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var itemDescriptionText: UITextField!
    @IBOutlet weak var itemNameText: UITextField!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var locationText: UITextField!
    
    var itemId: String!
    var currentItem: Item?
    
    var pin: MKPointAnnotation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let itemId = itemId {
            loadItemData(id: itemId)
        } else {
            addSaveButton()
        }
    }
    
    func loadMap(){
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        gestureRecognizer.delegate = self
        mapView.addGestureRecognizer(gestureRecognizer)
    }
    
    @objc func handleTap(gestureReconizer: UIGestureRecognizer){
        let location = gestureReconizer.location(in: mapView)
        let coordinate = mapView.convert(location,toCoordinateFrom: mapView)
        print(coordinate.latitude, coordinate.latitude)
        
        movePin(coordinate: coordinate)
        
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude), completionHandler: {places,error in
            self.locationText.text = "\(places?[0].name ?? "not found"), \(places?[0].locality ?? "not found"), \(places?[0].country ?? "not found")"
        })
    }
    
    func movePin(coordinate: CLLocationCoordinate2D){
        if pin == nil {
            pin = MKPointAnnotation()
        }
        pin?.coordinate = coordinate
        mapView.addAnnotation(pin!)
    }
    
    func zoomTo(coordinate: CLLocationCoordinate2D){
        let span = MKCoordinateSpan(latitudeDelta: 0.008, longitudeDelta: 0.008)
        let region = MKCoordinateRegion(center: coordinate, span: span)
        
        mapView.setRegion(region, animated: true)
    }
    
    func geocode(latitude: Double, longitude: Double, completion: @escaping (_ placemark: [CLPlacemark]?, _ error: Error?) -> Void)  {
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: latitude, longitude: longitude), completionHandler: completion)
    }
    
    func loadItemData(id: String){
        let db = Firestore.firestore()
        let householdId = CurrentHousehold.instance.document?.documentID
        db.collection("households").document(householdId!).collection("items").document(id).getDocument { (snapshot, error) in
            if let item = snapshot.flatMap({
                $0.data().flatMap({(data) in
                    return Item(dictionary: data)
                })
            }){
                self.currentItem = item
                let coordinate = CLLocationCoordinate2D(latitude: item.location?.lat ?? 0.0, longitude: item.location?.lng ?? 0.0)
                self.movePin(coordinate: coordinate)
                self.zoomTo(coordinate: coordinate)
                self.locationText.text = item.location?.name ?? "unknown"
                self.itemNameText.text = item.name ?? "unknown"
                self.itemDescriptionText.text = item.description ?? "unknown"
                let userId = Firebase.Auth.auth().currentUser?.uid
                if userId == item.owner?.reference?.documentID {
                    self.addSaveButton()
                } else {
                    self.addBuyButton()
                }
            }
        }
    }
    
    func addSaveButton(){
        let button = UIBarButtonItem(title: "Save", style: .done, target: self, action: #selector(saveButtonTapped))
        self.navigationItem.rightBarButtonItem = button
        loadMap()
    }
    
    @objc func saveButtonTapped(){
        let item = Item(dictionary: [
            "_id": itemId ?? "",
            "name": itemNameText.text ?? "none",
            "description": itemDescriptionText.text ?? "none",
            "location": [
                "lat": pin?.coordinate.latitude.magnitude ?? 0.0,
                "lng": pin?.coordinate.longitude.magnitude ?? 0.0,
                "name": locationText.text ?? "unknown"
                ]
            ])
        if let itemId = itemId {
            updateItem(item: item!)
        } else {
            createNewItem(item: item!)
        }
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func addBuyButton(){
        itemNameText.isEnabled = false
        itemDescriptionText.isEnabled = false
        locationText.isEnabled = false
        let button = UIBarButtonItem(title: "Buy", style: .done, target: self, action: #selector(buyButtonTapped))
        self.navigationItem.rightBarButtonItem = button
    }
    
    @objc func buyButtonTapped(){
        let item = Item(dictionary: [
            "_id": itemId ?? "",
            "name": itemNameText.text ?? "none",
            "description": itemDescriptionText.text ?? "none",
            "location": [
                "lat": pin?.coordinate.latitude.magnitude ?? 0.0,
                "lng": pin?.coordinate.longitude.magnitude ?? 0.0,
                "name": locationText.text ?? "unknown"
            ],
            "owner": [
                "name": currentItem?.owner?.name ?? "none",
                "reference": currentItem?.owner?.reference ?? nil
            ]
            ])
        buyItem(item: item!)
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func createNewItem(item: Item){
        let db = Firestore.firestore()
        let householdId = CurrentHousehold.instance.document?.documentID
        let userId = Auth.auth().currentUser?.uid
        db.collection("users").document(userId ?? "").getDocument {(snapshot, error) in
            let collection = db.collection("households").document(householdId ?? "").collection("items")
            let data: [String : Any] = [
                "_id": item._id ?? "",
                "name": item.name ?? "none",
                "description": item.description!,
                "location": [
                    "name" : item.location?.name! ?? "none",
                    "lat" : item.location?.lat! ?? 0.0,
                    "lng" : item.location?.lng! ?? 0.0
                ],
                "owner": [
                    "name": snapshot?.get("name") as? String ?? "",
                    "reference": snapshot?.reference ?? nil
                ],
                "status": "pending"
            ]
            collection.addDocument(data: data)
        }
    }
    
    func updateItem(item: Item){
        let db = Firestore.firestore()
        let householdId = CurrentHousehold.instance.document?.documentID
        let itemReference = db.collection("households").document(householdId ?? "").collection("items").document(item._id!)
        let data: [String : Any] = [
            "_id": item._id ?? "",
            "name": item.name ?? "none",
            "description": item.description!,
            "location": [
                "name" : item.location?.name! ?? "none",
                "lat" : item.location?.lat! ?? 0.0,
                "lng" : item.location?.lng! ?? 0.0
            ]
        ]
        itemReference.updateData(data)
    }
    
    func buyItem(item: Item){
        let db = Firestore.firestore()
        let householdId = CurrentHousehold.instance.document?.documentID
        let itemReference = db.collection("households").document(householdId ?? "").collection("items").document(item._id ?? "")
        itemReference.updateData(["status": "bought"])
        generateDebt(amount: 10.0, item: item)
    }
    
    func generateDebt(amount: Float, item: Item){
        let db = Firestore.firestore()
        let userId = Auth.auth().currentUser?.uid
        let userReference = db.collection("users").document(userId ?? "")
        let data: [String : Any] = [
            "_id": "",
            "amount": amount,
            "name": item.name ?? "Some item",
            "owner": [
                "name":item.owner?.name ?? "unknown",
                "reference":item.owner?.reference ?? nil
            ],
            "participants": userReference,
            "status": "pending"
        ]
        db.collection("households").document(CurrentHousehold.instance.document?.documentID ?? "").collection("debts").addDocument(data: data)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
